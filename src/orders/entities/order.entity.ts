import { Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { User } from "../../users/entities/user.entity";
import { Product } from "../../products/entities/product.entity";
import { RefOrderStatusCode } from "../../ref_order_status_codes/entities/ref_order_status_code.entity";

@Entity()
export class Order {
  @PrimaryGeneratedColumn({ name: "id" })
  id: number;
  @CreateDateColumn({ name: "created_at" })
  createdAt: Date;
  @UpdateDateColumn({ name: "updated_at" })
  updatedAt: Date;
  @Column({ name: "details", type: "text" })
  details: string;
  @ManyToOne(() => User, user => user.orders)
  @JoinColumn({ name: "user_id" })
  user: User;
  @ManyToOne(() => RefOrderStatusCode, refOrderStatusCode => refOrderStatusCode.orders)
  @JoinColumn({ name: "order_status_code" })
  refOrderStatusCode = RefOrderStatusCode;
  @ManyToMany(() => Product, product => product.orders)
  @JoinTable({
    name: "product_order",
    joinColumn: {
      name: "order_id",
      referencedColumnName: "id",
    },
    inverseJoinColumn: {
      name: "product_id",
      referencedColumnName: "id",
    },
  })
  products: Product[];
}
