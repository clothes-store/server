import { Injectable, NotFoundException } from "@nestjs/common";
import { CreateOrderDto } from "./dto/create-order.dto";
import { UpdateOrderDto } from "./dto/update-order.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Order } from "./entities/order.entity";
import { DeleteResult, Repository } from "typeorm";

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private readonly orderRepository: Repository<Order>,
  ) {}

  async create(createOrderDto: CreateOrderDto): Promise<Order> {
    const order: Order = this.orderRepository.create(createOrderDto);
    await this.orderRepository.save(order);
    return order;
  }

  async findAll(): Promise<Order[]> {
    return await this.orderRepository.find();
  }

  async findOne(id: number): Promise<Order> {
    return await this.orderRepository.findOne({ where: { id } });
  }

  async update(id: number, updateOrderDto: UpdateOrderDto): Promise<Order> {
    const order: Order = await this.orderRepository.findOne({ where: { id } });
    if (!order) throw new NotFoundException("Order is not found!");
    return await this.orderRepository.save({
      ...order,
      ...updateOrderDto,
    });
  }

  async removeOne(id: number): Promise<DeleteResult> {
    return await this.orderRepository.delete({ id });
  }

  async removeAll(): Promise<void> {
    const orders: Order[] = await this.orderRepository.find();
    if (!orders) {
      throw new NotFoundException("Orders are not founded!");
    }
    orders.forEach(order => {
      this.orderRepository.delete(order.id);
    });
  }
}
