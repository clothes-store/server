import * as process from "process";

export const config = () => ({
  port: parseInt(process.env.PORT, 10),
  secret: process.env.JWT_SECRET,
});
