import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModuleAsyncOptions, TypeOrmModuleOptions } from "@nestjs/typeorm";
import { DataSource } from "typeorm";

export default class TypeOrmConfig {
  static getOrmConfig(configService: ConfigService) {
    return {
      type: configService.get("DB_TYPE"),
      host: configService.get("DB_HOST"),
      port: configService.get("DB_PORT"),
      username: configService.get("DB_USER"),
      password: configService.get("DB_PASSWORD"),
      entities: [__dirname + "/../**/*.entity{.ts,.js}"],
      synchronize: true,
    };
  }
}
export const typeOrmConfigAsync: TypeOrmModuleAsyncOptions = {
  imports: [ConfigModule],
  useFactory: async (configService: ConfigService): Promise<TypeOrmModuleOptions> => TypeOrmConfig.getOrmConfig(configService),
  dataSourceFactory: async options => new DataSource(options).initialize(),
  inject: [ConfigService],
};
