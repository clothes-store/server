import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { config } from "./config/main.config";
import { typeOrmConfigAsync } from "./config/typeorm.config";
import { UsersModule } from "./users/users.module";
import { TokensModule } from "./tokens/tokens.module";
import { ProductsModule } from "./products/products.module";
import { CategoriesModule } from "./categories/categories.module";
import { InventoriesModule } from "./inventories/inventories.module";
import { TagsModule } from "./tags/tags.module";
import { RatingsModule } from "./ratings/ratings.module";
import { OrdersModule } from "./orders/orders.module";
import { RefOrderStatusCodesModule } from "./ref_order_status_codes/ref_order_status_codes.module";
import { RefInvoiceStatusCodesModule } from "./ref_invoice_status_codes/ref_invoice_status_codes.module";
import { InvoicesModule } from "./invoices/invoices.module";
import { ShipmentsModule } from "./shipments/shipments.module";
import { AuthModule } from "./auth/auth.module";
import { PassportModule } from "@nestjs/passport";
import { ServeStaticModule } from "@nestjs/serve-static";
import { join } from "path/posix";
import { SizesModule } from "./sizes/sizes.module";
import { HelpfulFeedbackModule } from "./helpful-feedback/helpful-feedback.module";

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, "..", "static"),
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
    TypeOrmModule.forRootAsync(typeOrmConfigAsync),
    UsersModule,
    TokensModule,
    ProductsModule,
    CategoriesModule,
    InventoriesModule,
    TagsModule,
    RatingsModule,
    OrdersModule,
    RefOrderStatusCodesModule,
    RefInvoiceStatusCodesModule,
    InvoicesModule,
    ShipmentsModule,
    AuthModule,
    PassportModule,
    SizesModule,
    HelpfulFeedbackModule,
  ],
  controllers: [AppController],
  providers: [AppService, ConfigService],
})
export class AppModule {}
