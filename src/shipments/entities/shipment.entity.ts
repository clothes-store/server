import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Order } from "../../orders/entities/order.entity";
import { Invoice } from "../../invoices/entities/invoice.entity";

@Entity()
export class Shipment {
  @PrimaryGeneratedColumn()
  id: number;
  @CreateDateColumn({ name: "created_at" })
  createdAt: Date;
  @UpdateDateColumn({ name: "updated_at" })
  updatedAt: Date;
  @Column({ name: "tracking_number", type: "int" })
  trackingNumber: number;
  @Column({ name: "ETA", type: "date" })
  ETA: Date;
  @Column({ name: "details", type: "text" })
  details: string;
  @OneToOne(() => Order, { cascade: true })
  @JoinColumn({ name: "order_id" })
  order: Order;
  @OneToOne(() => Invoice, { cascade: true })
  @JoinColumn({ name: "invoice_id" })
  invoice: Invoice;
}
