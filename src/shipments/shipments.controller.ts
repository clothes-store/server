import { Body, Controller, Delete, Get, Param, Patch, Post } from "@nestjs/common";
import { ShipmentsService } from "./shipments.service";
import { CreateShipmentDto } from "./dto/create-shipment.dto";
import { UpdateShipmentDto } from "./dto/update-shipment.dto";
import { DeleteResult } from "typeorm";
import { Shipment } from "./entities/shipment.entity";

@Controller("shipments")
export class ShipmentsController {
  constructor(private readonly shipmentsService: ShipmentsService) {}

  @Post()
  async create(@Body() createShipmentDto: CreateShipmentDto) {
    try {
      return await this.shipmentsService.create(createShipmentDto);
    } catch (error) {
      throw error;
    }
  }

  @Get()
  async findAll(): Promise<Shipment[]> {
    try {
      return await this.shipmentsService.findAll();
    } catch (error) {
      throw error;
    }
  }

  @Delete()
  async removeAll(): Promise<void> {
    try {
      return await this.shipmentsService.removeAll();
    } catch (error) {
      throw error;
    }
  }

  @Get(":id")
  async findOne(@Param("id") id: string): Promise<Shipment> {
    try {
      return await this.shipmentsService.findOneById(+id);
    } catch (error) {
      throw error;
    }
  }

  @Patch(":id")
  async update(@Param("id") id: string, @Body() updateShipmentDto: UpdateShipmentDto) {
    try {
      return await this.shipmentsService.update(+id, updateShipmentDto);
    } catch (error) {
      throw error;
    }
  }

  @Delete(":id")
  async remove(@Param("id") id: string): Promise<DeleteResult> {
    try {
      return await this.shipmentsService.remove(+id);
    } catch (error) {
      throw error;
    }
  }
}
