import { Injectable, NotFoundException } from "@nestjs/common";
import { CreateShipmentDto } from "./dto/create-shipment.dto";
import { UpdateShipmentDto } from "./dto/update-shipment.dto";
import { DeleteResult, Repository } from "typeorm";
import { Shipment } from "./entities/shipment.entity";
import { InjectRepository } from "@nestjs/typeorm";

@Injectable()
export class ShipmentsService {
  constructor(
    @InjectRepository(Shipment)
    private readonly shipmentRepository: Repository<Shipment>,
  ) {}

  async create(createShipmentDto: CreateShipmentDto): Promise<Shipment> {
    const newShipment: Shipment = this.shipmentRepository.create(createShipmentDto);
    return await this.shipmentRepository.save(newShipment);
  }

  async findAll(): Promise<Shipment[]> {
    return await this.shipmentRepository.find();
  }

  async findOneById(id: number): Promise<Shipment> {
    return await this.shipmentRepository.findOne({ where: { id } });
  }

  async findOneByTrackingNumber(trackingNumber: number): Promise<Shipment> {
    return await this.shipmentRepository.findOne({ where: { trackingNumber } });
  }

  async update(id: number, updateShipmentDto: UpdateShipmentDto): Promise<Shipment> {
    const shipment: Shipment = await this.shipmentRepository.findOne({ where: { id } });
    if (!shipment) {
      throw new NotFoundException("Shipment is not founded!");
    }
    return this.shipmentRepository.save({
      ...shipment,
      ...updateShipmentDto,
    });
  }

  async remove(id: number): Promise<DeleteResult> {
    return await this.shipmentRepository.delete({ id });
  }

  async removeAll(): Promise<void> {
    const shipments: Shipment[] = await this.shipmentRepository.find();
    if (!shipments) {
      throw new NotFoundException("Shipments are not founded!");
    }
    shipments.forEach(shipment => {
      this.shipmentRepository.delete(shipment.id);
    });
  }
}
