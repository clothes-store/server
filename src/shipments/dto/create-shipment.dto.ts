export class CreateShipmentDto {
  trackingNumber: number;
  ETA: Date;
  details: string;
  orderId?: number;
  invoiceId?: number;
}
