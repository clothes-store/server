import { Test, TestingModule } from "@nestjs/testing";
import { RefInvoiceStatusCodesController } from "./ref_invoice_status_codes.controller";
import { RefInvoiceStatusCodesService } from "./ref_invoice_status_codes.service";

describe("RefInvoiceStatusCodesController", () => {
  let controller: RefInvoiceStatusCodesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RefInvoiceStatusCodesController],
      providers: [RefInvoiceStatusCodesService],
    }).compile();

    controller = module.get<RefInvoiceStatusCodesController>(RefInvoiceStatusCodesController);
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
});
