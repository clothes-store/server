import { Test, TestingModule } from "@nestjs/testing";
import { RefInvoiceStatusCodesService } from "./ref_invoice_status_codes.service";

describe("RefInvoiceStatusCodesService", () => {
  let service: RefInvoiceStatusCodesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RefInvoiceStatusCodesService],
    }).compile();

    service = module.get<RefInvoiceStatusCodesService>(RefInvoiceStatusCodesService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
