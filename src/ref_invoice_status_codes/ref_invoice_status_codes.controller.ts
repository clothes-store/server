import { Controller, Get, Post, Body, Patch, Param, Delete } from "@nestjs/common";
import { RefInvoiceStatusCodesService } from "./ref_invoice_status_codes.service";
import { CreateRefInvoiceStatusCodeDto } from "./dto/create-ref_invoice_status_code.dto";
import { UpdateRefInvoiceStatusCodeDto } from "./dto/update-ref_invoice_status_code.dto";

@Controller("ref-invoice-status-codes")
export class RefInvoiceStatusCodesController {
  constructor(private readonly refInvoiceStatusCodesService: RefInvoiceStatusCodesService) {}

  @Post()
  create(@Body() createRefInvoiceStatusCodeDto: CreateRefInvoiceStatusCodeDto) {
    return this.refInvoiceStatusCodesService.create(createRefInvoiceStatusCodeDto);
  }

  @Get()
  findAll() {
    return this.refInvoiceStatusCodesService.findAll();
  }

  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.refInvoiceStatusCodesService.findOne(+id);
  }

  @Patch(":id")
  update(@Param("id") id: string, @Body() updateRefInvoiceStatusCodeDto: UpdateRefInvoiceStatusCodeDto) {
    return this.refInvoiceStatusCodesService.update(+id, updateRefInvoiceStatusCodeDto);
  }

  @Delete(":id")
  remove(@Param("id") id: string) {
    return this.refInvoiceStatusCodesService.remove(+id);
  }
}
