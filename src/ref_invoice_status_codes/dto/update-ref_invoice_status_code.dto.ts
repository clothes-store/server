import { PartialType } from "@nestjs/mapped-types";
import { CreateRefInvoiceStatusCodeDto } from "./create-ref_invoice_status_code.dto";

export class UpdateRefInvoiceStatusCodeDto extends PartialType(CreateRefInvoiceStatusCodeDto) {}
