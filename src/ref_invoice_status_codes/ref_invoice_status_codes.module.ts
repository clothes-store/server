import { Module } from "@nestjs/common";
import { RefInvoiceStatusCodesService } from "./ref_invoice_status_codes.service";
import { RefInvoiceStatusCodesController } from "./ref_invoice_status_codes.controller";

@Module({
  controllers: [RefInvoiceStatusCodesController],
  providers: [RefInvoiceStatusCodesService],
})
export class RefInvoiceStatusCodesModule {}
