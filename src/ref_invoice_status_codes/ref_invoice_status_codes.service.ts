import { Injectable } from "@nestjs/common";
import { CreateRefInvoiceStatusCodeDto } from "./dto/create-ref_invoice_status_code.dto";
import { UpdateRefInvoiceStatusCodeDto } from "./dto/update-ref_invoice_status_code.dto";

@Injectable()
export class RefInvoiceStatusCodesService {
  create(createRefInvoiceStatusCodeDto: CreateRefInvoiceStatusCodeDto) {
    return "This action adds a new refInvoiceStatusCode";
  }

  findAll() {
    return `This action returns all refInvoiceStatusCodes`;
  }

  findOne(id: number) {
    return `This action returns a #${id} refInvoiceStatusCode`;
  }

  update(id: number, updateRefInvoiceStatusCodeDto: UpdateRefInvoiceStatusCodeDto) {
    return `This action updates a #${id} refInvoiceStatusCode`;
  }

  remove(id: number) {
    return `This action removes a #${id} refInvoiceStatusCode`;
  }
}
