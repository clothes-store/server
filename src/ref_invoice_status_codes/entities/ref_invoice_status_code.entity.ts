import { Column, Entity, OneToMany, PrimaryColumn } from "typeorm";
import { Invoice } from "../../invoices/entities/invoice.entity";

@Entity()
export class RefInvoiceStatusCode {
  @PrimaryColumn({ name: "invoice_status_code", type: "varchar", nullable: false, default: "VOID" })
  invoiceStatusCode: string;
  @Column({ name: "invoice_status_description", type: "text" })
  invoiceStatusDescription: string;
  @OneToMany(() => Invoice, invoice => invoice.refInvoiceStatusCode, { cascade: true, onDelete: "CASCADE" })
  invoices: Invoice[];
}
