import { Body, Controller, Delete, Get, Param, Post, Query, Req, UploadedFiles, UseInterceptors } from "@nestjs/common";
import { ProductsService } from "./products.service";
import { CreateProductDto } from "./dto/create-product.dto";
import { Product } from "./entities/product.entity";
import { DeleteResult } from "typeorm";
import { diskStorage } from "multer";
import { FilesInterceptor } from "@nestjs/platform-express";
import { Request } from "express";

@Controller("products")
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post()
  @UseInterceptors(
    FilesInterceptor("images", 5, {
      storage: diskStorage({
        destination: "static",
        filename: (req, file, cb) => {
          cb(null, file.originalname);
        },
      }),
    }),
  )
  async create(@UploadedFiles() images: Express.Multer.File[], @Body() createProductDto: CreateProductDto): Promise<Product> {
    try {
      createProductDto.images = images;

      return await this.productsService.create(createProductDto);
    } catch (error) {
      throw error;
    }
  }

  @Get()
  async findAll(@Req() request: Request): Promise<Product[]> {
    const query = request.query;
    console.log(query);
    try {
      return await this.productsService.findAll(query);
    } catch (error) {
      throw error;
    }
  }

  @Delete()
  async removeAll(): Promise<void> {
    try {
      return await this.productsService.removeAll();
    } catch (error) {
      throw error;
    }
  }
  @Get("/prices")
  async a(): Promise<any> {
    try {
      return await this.productsService.findMinMaxPrices();
    } catch (error) {
      throw error;
    }
  }
  @Get(":id")
  async findOne(@Param("id") id: string): Promise<Product> {
    try {
      return await this.productsService.findOneById(+id);
    } catch (error) {
      throw error;
    }
  }

  @Delete(":id")
  async removeOne(@Param("id") id: string): Promise<DeleteResult> {
    try {
      return await this.productsService.removeOne(+id);
    } catch (error) {
      throw error;
    }
  }
}
