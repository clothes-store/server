import { Injectable, NotFoundException } from "@nestjs/common";
import { CreateProductDto } from "./dto/create-product.dto";
import { Between, DeleteResult, FindManyOptions, In, Repository } from "typeorm";
import { Product } from "./entities/product.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Size } from "../sizes/entities/size.entity";
import * as console from "console";

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
    @InjectRepository(Size)
    private readonly sizeRepository: Repository<Size>,
  ) {}

  async create(createProductDto: CreateProductDto): Promise<any> {
    const { images, sizes, ...productDTO } = createProductDto;
    const existingSizes = await this.sizeRepository.find({
      where: { size: In(sizes) },
    });

    if (existingSizes.length !== sizes.length) {
      throw new NotFoundException("One or more sizes do not exist.");
    }
    const imagesSrc = images.map(image => image.originalname);
    console.log(imagesSrc);
    const product: Product = this.productRepository.create({ images: imagesSrc, ...productDTO });
    await this.productRepository.save(product);
    await this.productRepository.createQueryBuilder().relation(Product, "sizes").of(product).add(existingSizes);

    return product;
  }
  async findAll(query: any): Promise<Product[]> {
    const { sizes, minPrice, maxPrice } = query;
    try {
      let options: FindManyOptions<Product> = {};
      if (sizes) {
        const sizesArray = query.sizes.split(",").map((size: string) => size.trim());
        options = {
          ...options,
          relations: ["sizes"],
          where: {
            sizes: {
              size: In(sizesArray),
            },
          },
        };
      }
      if (minPrice && maxPrice) {
        options = {
          ...options,
          where: {
            ...options.where,
            price: Between(minPrice, maxPrice),
          },
        };
      }
      const products = await this.productRepository.find(options);
      console.log(products);
      return products;
    } catch (error) {
      throw new Error(`Error fetching products: ${error.message}`);
    }
  }

  async findOneById(id: number): Promise<Product> {
    return await this.productRepository.findOne({ where: { id: +id }, relations: ["ratings"] });
  }

  async findMinMaxPrices() {
    try {
      const query = this.productRepository.createQueryBuilder("products");
      query.select("MIN(products.price)", "minPrice");
      query.addSelect("MAX(products.price)", "maxPrice");

      const result = await query.getRawOne();
      console.log(result);

      return result;
    } catch (error) {
      throw new Error(`Error finding min and max prices: ${error.message}`);
    }
  }
  // async update(id: number, updateProductDto: UpdateProductDto) {
  //   const product: Product = await this.productRepository.findOne({ where: { id } });
  //   if (!product) {
  //     throw new NotFoundException("Product is not found!");
  //   }
  //   return await this.productRepository.save({
  //     ...product,
  //     ...updateProductDto,
  //   });
  // }

  async removeOne(id: number): Promise<DeleteResult> {
    return await this.productRepository.delete({ id });
  }

  async removeAll(): Promise<void> {
    const products: Product[] = await this.productRepository.find();
    if (!products) {
      throw new NotFoundException("Products are not found!");
    }
    products.forEach(product => {
      this.productRepository.delete(product.id);
    });
  }
}
