import { Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Category } from "../../categories/entities/category.entity";
import { Tag } from "../../tags/entities/tag.entity";
import { Rating } from "../../ratings/entities/rating.entity";
import { Order } from "../../orders/entities/order.entity";
import { Size } from "../../sizes/entities/size.entity";

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;
  @CreateDateColumn({ name: "created_at" })
  createdAt: Date;
  @UpdateDateColumn({ name: "updated_at" })
  updatedAt: Date;
  @Column({ name: "title", type: "varchar" })
  title: string;
  @Column({ name: "price", type: "int" })
  price: number;
  @Column({ name: "color", type: "varchar" })
  color: string;
  @Column({ name: "material", type: "varchar" })
  material: string;
  @Column({ name: "manufacturer", type: "varchar" })
  manufacturer: string;
  @Column({ name: "description", type: "text" })
  description: string;
  @Column({ name: "images", array: true, type: "varchar" })
  images: string[];

  @ManyToOne(() => Category, category => category.products, { eager: true })
  categories: Category[];
  @ManyToMany(() => Tag, tag => tag.products)
  tags: Tag[];
  @ManyToMany(() => Order, order => order.products)
  @JoinTable({
    name: "product_order",
    joinColumn: {
      name: "product_id",
      referencedColumnName: "id",
    },
    inverseJoinColumn: {
      name: "order_id",
      referencedColumnName: "id",
    },
  })
  orders: Order[];
  @ManyToMany(() => Size, size => size.products, { eager: true, cascade: ["insert", "update", "remove"], onDelete: "CASCADE" })
  @JoinTable({
    name: "products_sizes",
    joinColumn: {
      name: "product_id",
      referencedColumnName: "id",
    },
    inverseJoinColumn: {
      name: "size_id",
      referencedColumnName: "id",
    },
  })
  sizes: Size[];

  @OneToMany(() => Rating, rating => rating.product)
  @JoinColumn({ name: "product_id" })
  ratings: Rating[];
}
