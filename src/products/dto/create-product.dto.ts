import { Size } from "../../sizes/entities/size.entity";

export class CreateProductDto {
  title: string;
  price: number;
  color: string;
  sizes: Size[];
  material: string;
  manufacturer: string;
  description: string;
  images: Express.Multer.File[];
}
