import { Injectable, NotFoundException } from "@nestjs/common";
import { CreateTagDto } from "./dto/create-tag.dto";
import { UpdateTagDto } from "./dto/update-tag.dto";
import { DeleteResult, Repository } from "typeorm";
import { Tag } from "./entities/tag.entity";
import { InjectRepository } from "@nestjs/typeorm";

@Injectable()
export class TagsService {
  constructor(
    @InjectRepository(Tag)
    private readonly tagsRepository: Repository<Tag>,
  ) {}

  async create(createTagDto: CreateTagDto): Promise<Tag> {
    try {
      const tag: Tag = this.tagsRepository.create(createTagDto);
      await this.tagsRepository.save(tag);
      return tag;
    } catch (e) {
      throw e;
    }
  }

  async findAll(): Promise<Tag[]> {
    return await this.tagsRepository.find();
  }

  async findOneById(id: number): Promise<Tag> {
    return await this.tagsRepository.findOne({ where: { id } });
  }

  async findOneByTag(tag: string): Promise<Tag> {
    return await this.tagsRepository.findOne({ where: { tag } });
  }

  async update(id: number, updateTagDto: UpdateTagDto): Promise<Tag> {
    const tag: Tag = await this.tagsRepository.findOne({ where: { id } });
    if (!tag) throw new NotFoundException("Tag is not found!");
    return await this.tagsRepository.save({
      ...tag,
      ...updateTagDto,
    });
  }

  async removeOne(id: number): Promise<DeleteResult> {
    return await this.tagsRepository.delete({ id });
  }

  async removeAll(): Promise<void> {
    const tags: Tag[] = await this.tagsRepository.find();
    if (!tags) throw new NotFoundException("Tags are not founded!");
    tags.forEach(tag => {
      this.tagsRepository.delete(tag.id);
    });
  }
}
