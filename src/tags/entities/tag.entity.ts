import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Product } from "../../products/entities/product.entity";

@Entity()
export class Tag {
  @PrimaryGeneratedColumn({ name: "id" })
  id: number;
  @Column({ name: "tag", type: "varchar" })
  tag: string;
  @ManyToMany(() => Product, product => product.tags)
  @JoinTable({
    name: "product_tag",
    joinColumn: {
      name: "product_id",
      referencedColumnName: "id",
    },
    inverseJoinColumn: {
      name: "tag_id",
      referencedColumnName: "id",
    },
  })
  products: Product[];
}
