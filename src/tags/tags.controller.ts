import { BadRequestException, Body, Controller, Delete, Get, Param, Patch, Post } from "@nestjs/common";
import { TagsService } from "./tags.service";
import { CreateTagDto } from "./dto/create-tag.dto";
import { UpdateTagDto } from "./dto/update-tag.dto";
import { Tag } from "./entities/tag.entity";
import { DeleteResult } from "typeorm";

@Controller("tags")
export class TagsController {
  constructor(private readonly tagsService: TagsService) {}

  @Post("/create")
  async create(@Body() createTagDto: CreateTagDto) {
    const isTagExist: boolean = !!(await this.tagsService.findOneByTag(createTagDto.tag));

    if (isTagExist) throw new BadRequestException("Tag already exist!");
    return this.tagsService.create(createTagDto);
  }

  @Get("/all")
  async findAll(): Promise<Tag[]> {
    try {
      return await this.tagsService.findAll();
    } catch (error) {
      throw error;
    }
  }

  @Get("/specific/:id")
  findOneById(@Param("id") id: string): Promise<Tag> {
    try {
      return this.tagsService.findOneById(+id);
    } catch (error) {
      throw error;
    }
  }

  @Patch("/specific/update/:id")
  update(@Param("id") id: string, @Body() updateTagDto: UpdateTagDto) {
    try {
      return this.tagsService.update(+id, updateTagDto);
    } catch (error) {
      throw error;
    }
  }

  @Delete("/delete/all")
  async removeAll(): Promise<void> {
    try {
      return await this.tagsService.removeAll();
    } catch (error) {
      throw error;
    }
  }

  @Delete("/delete/specific/:id")
  async remove(@Param("id") id: string): Promise<DeleteResult> {
    try {
      return await this.tagsService.removeOne(+id);
    } catch (error) {
      throw error;
    }
  }
}
