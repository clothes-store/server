import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Product } from "../../products/entities/product.entity";
import { User } from "../../users/entities/user.entity";
import { HelpfulFeedback } from "../../helpful-feedback/entities/helpful-feedback.entity";

@Entity()
export class Rating {
  @PrimaryGeneratedColumn({ name: "id" })
  id: number;
  @CreateDateColumn({ name: "created_at" })
  createdAt: Date;
  @UpdateDateColumn({ name: "updated_at" })
  updatedAt: Date;
  @Column({ name: "rating", type: "int" })
  rating: number;
  @Column({ name: "title", type: "text", nullable: true })
  title: string;
  @Column({ name: "review", type: "text" })
  review: string;
  @ManyToOne(() => Product, product => product.ratings, { eager: true })
  @JoinColumn({ name: "product_id" })
  product: Product;
  @ManyToOne(() => User, user => user.ratings, { eager: true })
  @JoinColumn({ name: "user_id" })
  user: User;
  @OneToMany(() => HelpfulFeedback, helpfulFeedback => helpfulFeedback.rating, { cascade: true, onDelete: "CASCADE" })
  @JoinColumn({ name: "rating_id" })
  helpfulFeedbacks: HelpfulFeedback[];
}
