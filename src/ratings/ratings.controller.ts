import { Body, Controller, Delete, Get, Param, Patch, Post, Req, UseGuards } from "@nestjs/common";
import { RatingsService } from "./ratings.service";
import { CreateRatingDto } from "./dto/create-rating.dto";
import { UpdateRatingDto } from "./dto/update-rating.dto";
import { Rating } from "./entities/rating.entity";
import { Request } from "express";
import { TokensService } from "../tokens/tokens.service";
import { AuthGuard } from "../auth/guards/auth.guard";

@Controller("ratings")
export class RatingsController {
  constructor(
    private readonly ratingsService: RatingsService,
    private readonly tokenService: TokensService,
  ) {}

  @UseGuards(AuthGuard)
  @Post()
  async create(@Body() createRatingDto: Partial<CreateRatingDto>, @Req() request: Request): Promise<Rating | any> {
    try {
      const { token } = request.cookies;
      const {
        user: { id: userId },
      } = await this.tokenService.findOneByToken(token);
      return await this.ratingsService.create({ ...createRatingDto, userId });
    } catch (error) {
      throw error;
    }
  }

  @Get()
  async findAll(): Promise<Rating[]> {
    try {
      return await this.ratingsService.findAll();
    } catch (error) {
      throw error;
    }
  }

  @Get(":id")
  async findOne(@Param("id") id: string) {
    try {
      return await this.ratingsService.findOneById(+id);
    } catch (error) {
      throw error;
    }
  }

  @Get("product/:id")
  async findAllByProductId(@Param("id") id: string) {
    try {
      return await this.ratingsService.findAllProductsReviewsSortedByPrice(+id, "DESC");
    } catch (error) {
      throw error;
    }
  }

  @Get("/get-all-by-rating")
  async findAllByRating(@Body("rating") rating: number) {
    try {
      return await this.ratingsService.findAllByRating(rating);
    } catch (error) {
      throw error;
    }
  }

  @Patch(":id")
  async update(@Param("id") id: string, @Body() updateRatingDto: UpdateRatingDto) {
    try {
      return await this.ratingsService.update(+id, updateRatingDto);
    } catch (error) {
      throw error;
    }
  }

  @Delete(":id")
  async remove(@Param("id") id: string) {
    try {
      return await this.ratingsService.remove(+id);
    } catch (error) {
      throw error;
    }
  }

  @Delete("")
  async removeAll() {
    try {
      return await this.ratingsService.removeAll();
    } catch (error) {
      throw error;
    }
  }
}
