export class CreateRatingDto {
  rating?: number;
  review?: string;
  userId?: number;
  productId?: number;
  title?: string;
  isHelpful?: boolean;
}
