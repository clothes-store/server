import { Injectable, NotFoundException } from "@nestjs/common";
import { UpdateRatingDto } from "./dto/update-rating.dto";
import { DeleteResult, Repository } from "typeorm";
import { Rating } from "./entities/rating.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { CreateRatingDto } from "./dto/create-rating.dto";
import { UsersService } from "../users/users.service";
import { User } from "../users/entities/user.entity";
import { ProductsService } from "../products/products.service";
import { Product } from "../products/entities/product.entity";

@Injectable()
export class RatingsService {
  constructor(
    @InjectRepository(Rating)
    private readonly ratingRepository: Repository<Rating>,
    private readonly usersService: UsersService,
    private readonly productsService: ProductsService,
  ) {}

  async create(createRatingDto: CreateRatingDto): Promise<any> {
    const rating: Rating = this.ratingRepository.create(createRatingDto);
    const user: User = await this.usersService.findOneById(createRatingDto.userId);
    const product: Product = await this.productsService.findOneById(createRatingDto.productId);
    rating.user = user;
    rating.product = product;
    return await this.ratingRepository.save(rating);
  }

  async findAll(): Promise<Rating[]> {
    return await this.ratingRepository.find();
  }

  async findAllProductsReviewsSortedByPrice(id: number, sortingBy: "ASC" | "DESC"): Promise<Rating[]> {
    return await this.ratingRepository.find({ where: { product: { id } }, order: { rating: sortingBy } });
  }

  async findOneById(id: number): Promise<Rating> {
    return await this.ratingRepository.findOne({ where: { id } });
  }

  async findAllByRating(rating: number): Promise<Rating[]> {
    return await this.ratingRepository.find({ where: { rating } });
  }

  async update(id: number, updateRatingDto: UpdateRatingDto) {
    const rating: Rating = await this.ratingRepository.findOne({ where: { id } });
    if (!rating) {
      throw new NotFoundException("Rating is not founded!");
    }
    return this.ratingRepository.save({
      ...rating,
      ...updateRatingDto,
    });
  }

  async remove(id: number): Promise<DeleteResult> {
    return await this.ratingRepository.delete({ id });
  }

  async removeAll(): Promise<void> {
    const ratings: Rating[] = await this.ratingRepository.find();
    if (!ratings) {
      throw new NotFoundException("Ratings are not founded!");
    }
    ratings.forEach(rating => {
      this.ratingRepository.delete(rating.id);
    });
  }
}
