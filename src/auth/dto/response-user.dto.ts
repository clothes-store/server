export default class ResponseUserDto {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  email: string;
  avatar: string;

  firstName: string;

  lastName: string;

  addressLine1: string;

  addressLine2: string;

  addressLine3: string;

  townCity: string;
  country: string;
  phone: number;
  accessToken: string;
}
