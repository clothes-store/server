import { UsersService } from "../users/users.service";
import { AuthService } from "./auth.service";
import { Injectable } from "@nestjs/common";
import { PassportSerializer } from "@nestjs/passport";
import { User } from "../users/entities/user.entity";

@Injectable()
export class LocalSerializer extends PassportSerializer {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UsersService,
  ) {
    super();
  }

  serializeUser(user: User, done: CallableFunction) {
    done(null, user.id);
  }

  async deserializeUser(email: string, done: CallableFunction) {
    return await this.userService
      .findOneByEmail(email)
      .then(user => done(null, user))
      .catch(error => done(error));
  }
}
