import { PassportStrategy } from "@nestjs/passport";
import { Strategy, VerifyCallback } from "passport-google-oauth20";
import { Injectable } from "@nestjs/common";
import { Profile } from "passport";
import { UsersService } from "../../users/users.service";

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, "google") {
  constructor(private readonly usersService: UsersService) {
    super({
      clientID: "471032779513-khdf6bsl0thfedkv6ht77s21tiiiigdt.apps.googleusercontent.com",
      clientSecret: "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",
      callbackURL: "http://localhost:3000/auth/google/redirect",
      scope: ["email", "profile"],
    });
  }

  async validate(accessToken: string, refreshToken: string, profile: Profile, done: VerifyCallback): Promise<any> {
    const user = await this.usersService.validateUser({
      email: profile.emails[0].value,
      password: "password",
      firstName: profile.name.givenName,
      avatar: profile.photos[0].value,
    });
    return user || null;
  }
}
