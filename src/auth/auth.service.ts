import { Injectable, UnauthorizedException } from "@nestjs/common";
import { SignInAuthDto } from "./dto/sign-in-auth.dto";
import { UsersService } from "../users/users.service";
import * as bcrypt from "bcrypt";

@Injectable()
export class AuthService {
  constructor(private readonly usersService: UsersService) {}

  async signIn(signInAuthDto: SignInAuthDto) {
    const { email, password } = signInAuthDto;
    const user = await this.usersService.findOneByEmail(email);
    if (!user) {
      throw new UnauthorizedException("Invalid email or password!");
    }
    const passwordIsValid = await bcrypt.compare(password, user.password);
    if (!passwordIsValid) {
      throw new UnauthorizedException("Invalid password!!!");
    }
    return user;
  }

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findOneByEmail(email);
    const passwordIsValid = await bcrypt.compare(password, user.password);
    if (user && passwordIsValid) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }
}
