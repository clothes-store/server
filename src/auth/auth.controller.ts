import { Body, Controller, Get, Post, Req, Res, UnauthorizedException, UseGuards } from "@nestjs/common";
import { SignUpAuthDto } from "./dto/sign-up-auth.dto";
import { UsersService } from "../users/users.service";
import { User } from "../users/entities/user.entity";
import { SignInAuthDto } from "./dto/sign-in-auth.dto";
import { AuthService } from "./auth.service";
import { TokensService } from "../tokens/tokens.service";
import { Public } from "./public.decorator";
import { AuthGuard } from "./guards/auth.guard";
import { Request, Response } from "express";
import { GoogleOAuthGuard } from "./guards/google-oauth.guard";

@Controller("auth")
export class AuthController {
  constructor(
    private readonly userService: UsersService,
    private readonly authService: AuthService,
    private readonly tokenService: TokensService,
  ) {}

  @Public()
  @Post("sign-up")
  async signUp(@Res() res: Response, @Body() signUpAuthDto: SignUpAuthDto) {
    const { email } = signUpAuthDto;
    const user: User = await this.userService.findOneByEmail(email);
    if (user) {
      throw new UnauthorizedException("User with following email already exist!");
    }
    const { password, activationLink, role, ...rest } = await this.userService.create(signUpAuthDto);
    const { accessToken, refreshToken } = await this.tokenService.generateTokens({ ...rest, role });
    res.cookie("token", refreshToken, { httpOnly: true });

    res.send({ ...rest, accessToken });
  }

  @Public()
  @Post("sign-in")
  async signIn(@Res() res: Response, @Body() signInAuthDto: SignInAuthDto) {
    const { password, activationLink, role, ...user } = await this.authService.signIn(signInAuthDto);
    const { accessToken, refreshToken } = await this.tokenService.generateTokens({ ...user, role });
    res.cookie("token", refreshToken);

    res.send({ ...user, accessToken });
  }

  @UseGuards(AuthGuard)
  @Post("refresh-token")
  async refreshToken() {
    return "Success";
  }

  @Get("google/login")
  @UseGuards(GoogleOAuthGuard)
  handleLogin(@Req() req: Request, @Res() res: Response) {}

  @Get("google/redirect")
  @UseGuards(GoogleOAuthGuard)
  async handleRedirect(@Req() req: Request, @Res() res: Response) {
    const user: Partial<User> = req.user;
    const { password, activationLink, role, ...rest } = user;
    const { accessToken, refreshToken } = await this.tokenService.generateTokens({ ...rest, role });
    res.cookie("token", refreshToken, { httpOnly: true });

    res.send({ ...rest, accessToken });
  }
}
