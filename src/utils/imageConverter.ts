import * as path from "path";
import * as bcrypt from "bcrypt";
import * as fs from "fs";
import * as sharp from "sharp";

export const convertImageToWebp = async (image: Express.Multer.File, distDir: string, oldPath: string, specificDataForHash: string): Promise<string> => {
  const uploadDir = path.join(__dirname, "../../static/", distDir);
  const salt = await bcrypt.genSalt(1);
  const hashed = await bcrypt.hash(specificDataForHash, salt);
  const newFileName = `${hashed}.webp`.replace(/\//g, "-");
  const filePath = path.join(uploadDir, newFileName);
  const oldFilePath = path.join(uploadDir, oldPath);
  if (!fs.existsSync(uploadDir)) {
    fs.mkdirSync(uploadDir, { recursive: true });
  }
  if (fs.existsSync(oldFilePath)) {
    fs.unlinkSync(oldFilePath);
  }
  try {
    const sharpImage = sharp(image.buffer);
    sharpImage.toFormat("webp");
    await sharpImage.toFile(filePath);
    return newFileName;
  } catch (e) {
    throw e;
  }
};
