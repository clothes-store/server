import { Injectable, NotFoundException } from "@nestjs/common";
import { CreateSizeDto } from "./dto/create-size.dto";
import { UpdateSizeDto } from "./dto/update-size.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, Repository } from "typeorm";
import { Size } from "./entities/size.entity";

@Injectable()
export class SizesService {
  constructor(
    @InjectRepository(Size)
    private readonly sizesRepository: Repository<Size>,
  ) {}

  async create(createSizeDto: CreateSizeDto): Promise<Size> {
    const size: Size = this.sizesRepository.create(createSizeDto);
    await this.sizesRepository.save(size);
    return size;
  }

  async findAll(): Promise<Size[]> {
    return await this.sizesRepository.find();
  }
  async findOneById(id: number): Promise<Size> {
    return await this.sizesRepository.findOne({ where: { id } });
  }

  async findOneBySize(size: string): Promise<Size> {
    return await this.sizesRepository.findOne({ where: { size } });
  }

  async update(id: number, updateSizeDto: UpdateSizeDto): Promise<Size> {
    const size: Size = await this.sizesRepository.findOne({ where: { id } });
    if (!size) throw new NotFoundException("Tag is not found!");
    return await this.sizesRepository.save({
      ...size,
      ...updateSizeDto,
    });
  }

  async removeOne(id: number): Promise<DeleteResult> {
    return await this.sizesRepository.delete({ id });
  }
  async removeAll(): Promise<void> {
    const sizes: Size[] = await this.sizesRepository.find();
    if (!sizes) throw new NotFoundException("Tags are not founded!");
    sizes.forEach(size => {
      this.sizesRepository.delete(size.id);
    });
  }
}
