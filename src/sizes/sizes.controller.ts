import { Body, Controller, Delete, Get, Param, Patch, Post } from "@nestjs/common";
import { SizesService } from "./sizes.service";
import { CreateSizeDto } from "./dto/create-size.dto";
import { UpdateSizeDto } from "./dto/update-size.dto";
import { DeleteResult } from "typeorm";
import { Size } from "./entities/size.entity";

@Controller("sizes")
export class SizesController {
  constructor(private readonly sizesService: SizesService) {}

  @Post("/create")
  create(@Body() createSizeDto: CreateSizeDto): Promise<Size> {
    return this.sizesService.create(createSizeDto);
  }

  @Get("/all")
  async findAll(): Promise<Size[]> {
    return await this.sizesService.findAll();
  }

  @Get("/specific/:id")
  findOne(@Param("id") id: string): Promise<Size> {
    return this.sizesService.findOneById(+id);
  }

  @Patch("/specific/update/:id")
  update(@Param("id") id: string, @Body() updateSizeDto: UpdateSizeDto): Promise<Size> {
    return this.sizesService.update(+id, updateSizeDto);
  }
  @Delete("/delete/all")
  removeAll(): Promise<void> {
    return this.sizesService.removeAll();
  }
  @Delete("/specific/delete/:id")
  remove(@Param("id") id: string): Promise<DeleteResult> {
    return this.sizesService.removeOne(+id);
  }
}
