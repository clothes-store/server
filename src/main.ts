import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import * as process from "process";
import { config } from "dotenv";
import * as cookieParser from "cookie-parser";
import * as session from "express-session";

config();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(cookieParser());
  app.enableCors();
  app.use(
    session({
      secret: "secret",
      resave: false,
      saveUninitialized: false,
    }),
  );
  await app.listen(process.env.PORT || 3000);
}

bootstrap();
