import { Column, CreateDateColumn, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Rating } from "../../ratings/entities/rating.entity";
import { Order } from "../../orders/entities/order.entity";
import { HelpfulFeedback } from "../../helpful-feedback/entities/helpful-feedback.entity";

export enum UserRole {
  USER = "USER",
  MANAGER = "MANAGER",
  ADMIN = "ADMIN",
}

@Entity()
export class User {
  @PrimaryGeneratedColumn({ name: "id" })
  id: number;
  @CreateDateColumn({ name: "created_at" })
  createdAt: Date;
  @UpdateDateColumn({ name: "updated_at" })
  updatedAt: Date;
  @Column({ type: "citext", nullable: false })
  email: string;
  @Column({ type: "varchar", nullable: false })
  password: string;
  @Column({
    name: "avatar",
    type: "text",
    default: "https://kis.agh.edu.pl/wp-content/uploads/2021/01/default-avatar.jpg",
  })
  avatar: string;
  @Column({ name: "first_name", type: "varchar", nullable: true })
  firstName: string;
  @Column({ name: "last_name", type: "varchar", nullable: true })
  lastName: string;
  @Column({ name: "address_line_1", type: "text", nullable: true })
  addressLine1: string;
  @Column({ name: "address_line_2", type: "text", nullable: true })
  addressLine2: string;
  @Column({ name: "address_line_3", type: "text", nullable: true })
  addressLine3: string;
  @Column({ name: "town_city", type: "varchar", nullable: true })
  townCity: string;
  @Column({ name: "country", type: "varchar", nullable: true })
  country: string;
  @Column({ name: "phone", type: "bigint", nullable: true })
  phone: number;
  @Column({ name: "activation_link", type: "text", nullable: true })
  activationLink: string;
  @Column({ name: "role", type: "enum", enum: UserRole, default: UserRole.USER })
  role: UserRole;
  @OneToMany(() => Rating, rating => rating.user, { cascade: true, onDelete: "CASCADE" })
  @JoinColumn({ name: "user_id" })
  ratings: Rating[];
  @OneToMany(() => Order, order => order.user)
  @JoinColumn({ name: "user_id" })
  orders: Order[];
  @OneToMany(() => HelpfulFeedback, helpfulFeedback => helpfulFeedback.user, { cascade: true, onDelete: "CASCADE" })
  @JoinColumn({ name: "user_id" })
  helpfulFeedbacks: HelpfulFeedback[];
}
