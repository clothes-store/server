import { BadRequestException, Injectable } from "@nestjs/common";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { TokensService } from "../tokens/tokens.service";
import { DeleteResult, Repository } from "typeorm";
import { User } from "./entities/user.entity";
import { InjectRepository } from "@nestjs/typeorm";
import * as bcrypt from "bcrypt";
import { convertImageToWebp } from "../utils/imageConverter";

@Injectable()
export class UsersService {
  constructor(
    private readonly tokenService: TokensService,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(createUserDto.password, salt);
    const user = this.userRepository.create({ ...createUserDto, password: hashedPassword });
    await this.userRepository.save(user);
    return user;
  }

  async findAll(): Promise<User[]> {
    return await this.userRepository.find();
  }

  findOneById(id: number): Promise<User> {
    return this.userRepository.findOne({ where: { id } });
  }

  findOneByEmail(email: string): Promise<User> {
    return this.userRepository.findOne({ where: { email } });
  }

  async update(id: number, updateUserDto: UpdateUserDto, file: Express.Multer.File) {
    const user: User = await this.userRepository.findOne({ where: { id } });
    if (!user) {
      throw new BadRequestException("User doesn't exist!");
    }
    if (file) {
      user.avatar = await convertImageToWebp(file, "users", user.avatar, user.email);
      return await this.userRepository.save({
        ...user,
        ...updateUserDto,
      });
    }

    return await this.userRepository.save({
      ...user,
      ...updateUserDto,
    });
  }

  async removeOne(id: number): Promise<DeleteResult> {
    return await this.userRepository.delete({ id });
  }

  async removeAll(): Promise<void> {
    const users = await this.userRepository.find();
    users.forEach(user => {
      this.userRepository.delete(user.id);
    });
  }

  async validateUser(userInfo) {
    const user = await this.userRepository.findOne({ where: { email: userInfo.email } });
    if (user) return user;
    const salt = await bcrypt.genSalt();
    const generatedPassword = await bcrypt.hash(userInfo.email, salt);
    const newUser = this.userRepository.create({ ...userInfo, password: generatedPassword });
    return await this.userRepository.save(newUser);
  }
}
