import { Controller, Get, Post, Body, Patch, Param, Delete, BadRequestException } from "@nestjs/common";
import { CategoriesService } from "./categories.service";
import { CreateCategoryDto } from "./dto/create-category.dto";
import { UpdateCategoryDto } from "./dto/update-category.dto";
import { DeleteResult } from "typeorm";
import { Category } from "./entities/category.entity";

@Controller("categories")
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @Post("/create")
  async create(@Body() createCategoryDto: CreateCategoryDto): Promise<Category> {
    const isCategoryExist: boolean = !!(await this.categoriesService.findOneByName(createCategoryDto.category));

    if (isCategoryExist) throw new BadRequestException("Category already exist");
    return this.categoriesService.create(createCategoryDto);
  }

  @Get("/all")
  findAll(): Promise<Category[]> {
    return this.categoriesService.findAll();
  }

  @Get("/specific/:id")
  findOneByName(@Param("id") id: string): Promise<Category> {
    return this.categoriesService.findOneById(+id);
  }

  @Patch("/specific/update/:id")
  update(@Param("id") id: string, @Body() updateCategoryDto: UpdateCategoryDto): Promise<Category> {
    return this.categoriesService.update(+id, updateCategoryDto);
  }
  @Delete("/delete/all")
  removeAll(): Promise<void> {
    return this.categoriesService.removeAll();
  }
  @Delete("/delete/specific/:id")
  remove(@Param("id") id: string): Promise<DeleteResult> {
    return this.categoriesService.remove(+id);
  }
}
