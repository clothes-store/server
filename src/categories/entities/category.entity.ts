import { Column, CreateDateColumn, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Product } from "../../products/entities/product.entity";

@Entity()
export class Category {
  @PrimaryGeneratedColumn({ name: "id" })
  id: number;
  @CreateDateColumn({ name: "created_at" })
  createdAt: Date;
  @UpdateDateColumn({ name: "updated_at" })
  updatedAt: Date;
  @Column({ name: "category", type: "varchar" })
  category: string;
  @OneToMany(() => Product, product => product.categories)
  @JoinColumn({
    name: "product_id",
    referencedColumnName: "id",
  })
  products: Product[];
}
