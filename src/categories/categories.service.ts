import { BadRequestException, Injectable, NotFoundException } from "@nestjs/common";
import { CreateCategoryDto } from "./dto/create-category.dto";
import { UpdateCategoryDto } from "./dto/update-category.dto";
import { Category } from "./entities/category.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, Repository } from "typeorm";

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(Category)
    private readonly categoryRepository: Repository<Category>,
  ) {}

  async create(createCategoryDto: CreateCategoryDto): Promise<Category> {
    try {
      const category: Category = this.categoryRepository.create(createCategoryDto);
      await this.categoryRepository.save(category);
      return category;
    } catch (e) {
      throw e;
    }
  }

  findAll(): Promise<Category[]> {
    return this.categoryRepository.find();
  }

  findOneById(id: number): Promise<Category> {
    return this.categoryRepository.findOne({ where: { id } });
  }
  findOneByName(category: string): Promise<Category> {
    return this.categoryRepository.findOne({ where: { category } });
  }
  async update(id: number, updateCategoryDto: UpdateCategoryDto): Promise<Category> {
    const category: Category = await this.categoryRepository.findOne({ where: { id } });
    if (!category) throw new BadRequestException("Category doesn't exist!");
    return await this.categoryRepository.save({
      ...category,
      updateCategoryDto,
    });
  }

  async remove(id: number): Promise<DeleteResult> {
    return await this.categoryRepository.delete({ id });
  }
  async removeAll(): Promise<void> {
    const categories: Category[] = await this.categoryRepository.find();
    if (!categories) throw new NotFoundException("Categories are not founded!");
    categories.forEach(category => {
      this.categoryRepository.delete(category.id);
    });
  }
}
