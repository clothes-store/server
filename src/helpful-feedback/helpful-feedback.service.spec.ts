import { Test, TestingModule } from "@nestjs/testing";
import { HelpfulFeedbackService } from "./helpful-feedback.service";

describe("HelpfulFeedbackService", () => {
  let service: HelpfulFeedbackService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HelpfulFeedbackService],
    }).compile();

    service = module.get<HelpfulFeedbackService>(HelpfulFeedbackService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
