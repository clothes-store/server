import { Test, TestingModule } from "@nestjs/testing";
import { HelpfulFeedbackController } from "./helpful-feedback.controller";
import { HelpfulFeedbackService } from "./helpful-feedback.service";

describe("HelpfulFeedbackController", () => {
  let controller: HelpfulFeedbackController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HelpfulFeedbackController],
      providers: [HelpfulFeedbackService],
    }).compile();

    controller = module.get<HelpfulFeedbackController>(HelpfulFeedbackController);
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
});
