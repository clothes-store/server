import { Module } from "@nestjs/common";
import { HelpfulFeedbackService } from "./helpful-feedback.service";
import { HelpfulFeedbackController } from "./helpful-feedback.controller";
import { RatingsModule } from "../ratings/ratings.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { HelpfulFeedback } from "./entities/helpful-feedback.entity";
import { TokensModule } from "../tokens/tokens.module";
import { UsersModule } from "../users/users.module";

@Module({
  imports: [TypeOrmModule.forFeature([HelpfulFeedback]), TokensModule, RatingsModule, UsersModule],
  controllers: [HelpfulFeedbackController],
  providers: [HelpfulFeedbackService],
})
export class HelpfulFeedbackModule {}
