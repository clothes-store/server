import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { User } from "../../users/entities/user.entity";
import { Rating } from "../../ratings/entities/rating.entity";

@Entity()
export class HelpfulFeedback {
  @PrimaryGeneratedColumn({ name: "id" })
  id: number;
  @CreateDateColumn({ name: "created_at" })
  createdAt: Date;
  @UpdateDateColumn({ name: "updated_at" })
  updatedAt: Date;
  @Column({ name: "isHelpful", type: "boolean" })
  isHelpful: boolean;
  @ManyToOne(() => User, user => user.helpfulFeedbacks, { eager: true })
  @JoinColumn({ name: "user_id" })
  user: User;
  @ManyToOne(() => Rating, rating => rating.helpfulFeedbacks, { eager: true })
  @JoinColumn({ name: "rating_id" })
  rating: Rating;
}
