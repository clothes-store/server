import { ConflictException, Injectable } from "@nestjs/common";
import { CreateHelpfulFeedbackDto } from "./dto/create-helpful-feedback.dto";
import { UpdateHelpfulFeedbackDto } from "./dto/update-helpful-feedback.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { HelpfulFeedback } from "./entities/helpful-feedback.entity";
import { UsersService } from "../users/users.service";
import { RatingsService } from "../ratings/ratings.service";

@Injectable()
export class HelpfulFeedbackService {
  constructor(
    @InjectRepository(HelpfulFeedback)
    private readonly helpfulFeedbackRepository: Repository<HelpfulFeedback>,
    private readonly usersService: UsersService,
    private readonly ratingsService: RatingsService,
  ) {}

  async create(createHelpfulFeedbackDto: CreateHelpfulFeedbackDto) {
    const user = await this.usersService.findOneById(createHelpfulFeedbackDto.userId);
    const rating = await this.ratingsService.findOneById(createHelpfulFeedbackDto.ratingId);
    const existingFeedback = await this.helpfulFeedbackRepository.findOne({
      where: {
        user: { id: createHelpfulFeedbackDto.userId },
        rating: { id: createHelpfulFeedbackDto.ratingId },
      },
    });

    if (existingFeedback) {
      throw new ConflictException("Feedback already exists for this user and rating");
    }
    const res = this.helpfulFeedbackRepository.create(createHelpfulFeedbackDto);
    res.user = user;
    res.rating = rating;
    await this.helpfulFeedbackRepository.save(res);
    return res;
  }

  async findAll() {
    return await this.helpfulFeedbackRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} helpfulFeedback`;
  }

  update(id: number, updateHelpfulFeedbackDto: UpdateHelpfulFeedbackDto) {
    return `This action updates a #${id} helpfulFeedback`;
  }

  remove(id: number) {
    return `This action removes a #${id} helpfulFeedback`;
  }
}
