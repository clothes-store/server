import { Body, Controller, Delete, Get, Param, Patch, Post, Req } from "@nestjs/common";
import { HelpfulFeedbackService } from "./helpful-feedback.service";
import { CreateHelpfulFeedbackDto } from "./dto/create-helpful-feedback.dto";
import { UpdateHelpfulFeedbackDto } from "./dto/update-helpful-feedback.dto";
import { Request } from "express";
import { TokensService } from "../tokens/tokens.service";
import { RatingsService } from "../ratings/ratings.service";

@Controller("helpful-feedback")
export class HelpfulFeedbackController {
  constructor(
    private readonly helpfulFeedbackService: HelpfulFeedbackService,
    private readonly tokenService: TokensService,
    private readonly ratingsService: RatingsService,
  ) {}

  @Post()
  async create(@Body() createHelpfulFeedbackDto: Partial<CreateHelpfulFeedbackDto>, @Req() request: Request) {
    try {
      const { token } = request.cookies;
      const {
        user: { id: userId },
      } = await this.tokenService.findOneByToken(token);
      return this.helpfulFeedbackService.create({ ...createHelpfulFeedbackDto, userId });
    } catch (error) {
      throw error;
    }
  }

  @Get()
  findAll() {
    return this.helpfulFeedbackService.findAll();
  }

  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.helpfulFeedbackService.findOne(+id);
  }

  @Patch(":id")
  update(@Param("id") id: string, @Body() updateHelpfulFeedbackDto: UpdateHelpfulFeedbackDto) {
    return this.helpfulFeedbackService.update(+id, updateHelpfulFeedbackDto);
  }

  @Delete(":id")
  remove(@Param("id") id: string) {
    return this.helpfulFeedbackService.remove(+id);
  }
}
