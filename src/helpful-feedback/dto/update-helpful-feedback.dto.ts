import { PartialType } from "@nestjs/mapped-types";
import { CreateHelpfulFeedbackDto } from "./create-helpful-feedback.dto";

export class UpdateHelpfulFeedbackDto extends PartialType(CreateHelpfulFeedbackDto) {}
