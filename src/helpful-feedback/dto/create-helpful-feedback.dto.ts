export class CreateHelpfulFeedbackDto {
  ratingId?: number;
  userId?: number;
  isHelpful?: boolean;
}
