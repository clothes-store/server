import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { RefInvoiceStatusCode } from "../../ref_invoice_status_codes/entities/ref_invoice_status_code.entity";
import { Order } from "../../orders/entities/order.entity";

@Entity()
export class Invoice {
  @PrimaryGeneratedColumn()
  id: number;
  @CreateDateColumn({ name: "created_at" })
  createdAt: Date;
  @UpdateDateColumn({ name: "updated_at" })
  updatedAt: Date;
  @Column({ name: "invoice_details", type: "text" })
  invoiceDetails: string;
  @ManyToOne(() => RefInvoiceStatusCode, refInvoiceStatusCode => refInvoiceStatusCode.invoices)
  @JoinColumn({ name: "invoice_status_code" })
  refInvoiceStatusCode = RefInvoiceStatusCode;
  @OneToOne(() => Order, { cascade: true, onDelete: "CASCADE" })
  @JoinColumn({ name: "order_id" })
  order: Order;
}
