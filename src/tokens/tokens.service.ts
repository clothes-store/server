import { BadRequestException, Injectable, UnauthorizedException } from "@nestjs/common";
import { CreateTokenDto } from "./dto/create-token.dto";
import { UpdateTokenDto } from "./dto/update-token.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Token } from "./entities/token.entity";
import { Repository } from "typeorm";
import { JwtService } from "@nestjs/jwt";
import { User } from "../users/entities/user.entity";

@Injectable()
export class TokensService {
  constructor(
    @InjectRepository(Token)
    private readonly tokenRepository: Repository<Token>,
    private readonly jwtService: JwtService,
  ) {}

  async generateTokens(data: Partial<User>) {
    const payload = { id: data.id, email: data.email, role: data.role };
    const accessToken = await this.jwtService.signAsync(payload, { expiresIn: "50s" });
    const refreshToken = await this.jwtService.signAsync(payload, { expiresIn: "7d" });
    const tokenFromDB = await this.tokenRepository.findOne({ where: { user: { id: data.id } } });
    if (!tokenFromDB) {
      const tokenInDB = this.tokenRepository.create({ token: refreshToken, user: data });
      await this.tokenRepository.save(tokenInDB);
    } else {
      tokenFromDB.token = refreshToken;
      await this.tokenRepository.save(tokenFromDB);
    }
    return { accessToken, refreshToken };
  }

  async validateRefreshToken(id, token) {
    const tokenFromDB = await this.tokenRepository.findOne({ where: { user: { id } } });
    if (tokenFromDB !== token) {
      throw new BadRequestException("Invalid refresh token! Please sign in again!");
    }
  }

  async refreshAccessToken(refreshToken: string): Promise<{ accessToken: string }> {
    try {
      const decoded = await this.jwtService.verifyAsync(refreshToken);
      await this.validateRefreshToken(decoded.id, refreshToken);
      const accessToken = await this.jwtService.signAsync(decoded);
      return { accessToken };
    } catch (e) {
      throw new UnauthorizedException("Invalid refresh token!");
    }
  }

  create(createTokenDto: CreateTokenDto) {
    return "This action adds a new token";
  }

  findAll() {
    return `This action returns all tokens`;
  }

  async findOne(id: number) {
    return await this.tokenRepository.findOne({ where: { id } });
  }

  async findOneByToken(token: string) {
    return await this.tokenRepository.findOne({ where: { token } });
  }

  update(id: number, updateTokenDto: UpdateTokenDto) {
    return `This action updates a #${id} token`;
  }

  async remove(data: User) {
    return await this.tokenRepository.delete({ user: { id: data.id } });
  }
}
