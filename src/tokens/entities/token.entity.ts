import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { User } from "../../users/entities/user.entity";

@Entity()
export class Token {
  @PrimaryGeneratedColumn({ name: "id" })
  id: number;
  @CreateDateColumn({ name: "created_at" })
  createdAt: Date;
  @UpdateDateColumn({ name: "updated_at" })
  updatedAt: Date;
  @Column({ name: "token", type: "text" })
  token: string;
  @OneToOne(() => User, { cascade: true, onDelete: "CASCADE", eager: true })
  @JoinColumn({ name: "user_id" })
  user: User;
}
