import { Column, Entity, OneToMany, PrimaryColumn } from "typeorm";
import { Order } from "../../orders/entities/order.entity";

@Entity()
export class RefOrderStatusCode {
  @PrimaryColumn({ name: "order_status_code", type: "varchar", nullable: false, default: "VOID" })
  orderStatusCode: string;
  @Column({ name: "order_status_description", type: "text" })
  orderStatusDescription: string;
  @OneToMany(() => Order, order => order.refOrderStatusCode, { cascade: true, onDelete: "CASCADE" })
  orders: Order[];
}
