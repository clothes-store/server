import { Test, TestingModule } from "@nestjs/testing";
import { RefOrderStatusCodesController } from "./ref_order_status_codes.controller";
import { RefOrderStatusCodesService } from "./ref_order_status_codes.service";

describe("RefOrderStatusCodesController", () => {
  let controller: RefOrderStatusCodesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RefOrderStatusCodesController],
      providers: [RefOrderStatusCodesService],
    }).compile();

    controller = module.get<RefOrderStatusCodesController>(RefOrderStatusCodesController);
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
});
