import { Injectable } from "@nestjs/common";
import { CreateRefOrderStatusCodeDto } from "./dto/create-ref_order_status_code.dto";
import { UpdateRefOrderStatusCodeDto } from "./dto/update-ref_order_status_code.dto";

@Injectable()
export class RefOrderStatusCodesService {
  create(createRefOrderStatusCodeDto: CreateRefOrderStatusCodeDto) {
    return "This action adds a new refOrderStatusCode";
  }

  findAll() {
    return `This action returns all refOrderStatusCodes`;
  }

  findOne(id: number) {
    return `This action returns a #${id} refOrderStatusCode`;
  }

  update(id: number, updateRefOrderStatusCodeDto: UpdateRefOrderStatusCodeDto) {
    return `This action updates a #${id} refOrderStatusCode`;
  }

  remove(id: number) {
    return `This action removes a #${id} refOrderStatusCode`;
  }
}
