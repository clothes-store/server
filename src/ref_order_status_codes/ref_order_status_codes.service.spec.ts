import { Test, TestingModule } from "@nestjs/testing";
import { RefOrderStatusCodesService } from "./ref_order_status_codes.service";

describe("RefOrderStatusCodesService", () => {
  let service: RefOrderStatusCodesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RefOrderStatusCodesService],
    }).compile();

    service = module.get<RefOrderStatusCodesService>(RefOrderStatusCodesService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
