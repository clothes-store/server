import { PartialType } from "@nestjs/mapped-types";
import { CreateRefOrderStatusCodeDto } from "./create-ref_order_status_code.dto";

export class UpdateRefOrderStatusCodeDto extends PartialType(CreateRefOrderStatusCodeDto) {}
