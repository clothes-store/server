import { Controller, Get, Post, Body, Patch, Param, Delete } from "@nestjs/common";
import { RefOrderStatusCodesService } from "./ref_order_status_codes.service";
import { CreateRefOrderStatusCodeDto } from "./dto/create-ref_order_status_code.dto";
import { UpdateRefOrderStatusCodeDto } from "./dto/update-ref_order_status_code.dto";

@Controller("ref-order-status-codes")
export class RefOrderStatusCodesController {
  constructor(private readonly refOrderStatusCodesService: RefOrderStatusCodesService) {}

  @Post()
  create(@Body() createRefOrderStatusCodeDto: CreateRefOrderStatusCodeDto) {
    return this.refOrderStatusCodesService.create(createRefOrderStatusCodeDto);
  }

  @Get()
  findAll() {
    return this.refOrderStatusCodesService.findAll();
  }

  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.refOrderStatusCodesService.findOne(+id);
  }

  @Patch(":id")
  update(@Param("id") id: string, @Body() updateRefOrderStatusCodeDto: UpdateRefOrderStatusCodeDto) {
    return this.refOrderStatusCodesService.update(+id, updateRefOrderStatusCodeDto);
  }

  @Delete(":id")
  remove(@Param("id") id: string) {
    return this.refOrderStatusCodesService.remove(+id);
  }
}
