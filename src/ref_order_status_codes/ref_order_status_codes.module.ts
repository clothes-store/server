import { Module } from "@nestjs/common";
import { RefOrderStatusCodesService } from "./ref_order_status_codes.service";
import { RefOrderStatusCodesController } from "./ref_order_status_codes.controller";

@Module({
  controllers: [RefOrderStatusCodesController],
  providers: [RefOrderStatusCodesService],
})
export class RefOrderStatusCodesModule {}
