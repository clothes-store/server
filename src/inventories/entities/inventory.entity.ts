import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Product } from "../../products/entities/product.entity";

@Entity()
export class Inventory {
  @PrimaryGeneratedColumn({ name: "id" })
  id: number;
  @CreateDateColumn({ name: "created_at" })
  createdAt: Date;
  @UpdateDateColumn({ name: "updated_at" })
  updatedAt: Date;
  @Column({ name: "amount", type: "int" })
  amount: string;
  @OneToOne(() => Product, { cascade: true })
  @JoinColumn({ name: "product_id" })
  product: Product;
}
